import { getPathArraysFromPaths } from '../lib';
import { getResourceTreeFromPathArrays } from '../lib';

describe('Resource Tree From Config', () => {
  it('should create a tree from a valid config', () => {
    const validPaths = ['a', 'a/aa', 'a/ab', 'b', 'b/ba', 'b/bb', 'b/bb/bba'];
    const resourceTree = getResourceTreeFromPathArrays(getPathArraysFromPaths(validPaths));

    expect(resourceTree).toEqual({
      a: {
        aa: {},
        ab: {},
      },
      b: {
        ba: {},
        bb: {
          bba: {},
        },
      },
    });
  });
});
