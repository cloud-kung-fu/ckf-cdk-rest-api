import {
  AuthorizationType,
  CfnAuthorizer,
  IAuthorizer,
  IdentitySource,
  RequestAuthorizer,
  RequestAuthorizerProps,
  TokenAuthorizer,
  TokenAuthorizerProps,
} from '@aws-cdk/aws-apigateway';
import { Construct, Duration } from '@aws-cdk/core';
import {
  CognitoAuthorizerConfig,
  getLambdaFunctionProps,
  RequestAuthorizerConfig,
  TokenAuthorizerConfig,
} from '../lib';
import { LambdaFunction } from '../lambda-function';
import { Function } from '@aws-cdk/aws-lambda';
import { CkfApiAuthorizerProps } from './index';

interface AuthorizerProps {
  readonly handler: Function;
  readonly identitySources: string[];
  readonly authorizationType: AuthorizationType;
  readonly resultsCacheTtl?: Duration;
  readonly authorizerName?: string;
}

const getAuthorizerProps = (scope: Construct, authorizerProps: CkfApiAuthorizerProps): AuthorizerProps => {
  const requestOrTokenAuthorizerConfig = authorizerProps.config as RequestAuthorizerConfig | TokenAuthorizerConfig;

  const lambdaFunction: LambdaFunction = new LambdaFunction(
    scope,
    `${authorizerProps.id}AuthorizerLambdaFunction`,
    getLambdaFunctionProps(requestOrTokenAuthorizerConfig.function, authorizerProps.appRestApi)
  );

  const resultsCacheTtl: Duration | undefined = requestOrTokenAuthorizerConfig.resultsCacheTtlSeconds
    ? Duration.seconds(requestOrTokenAuthorizerConfig.resultsCacheTtlSeconds)
    : undefined;

  const authorizerName: string | undefined = authorizerProps.name;

  return {
    handler: lambdaFunction.function,
    // TODO: Support other identity sources
    identitySources: authorizerProps.authHeaders.map((authHeader) => IdentitySource.header(authHeader)),
    authorizationType: AuthorizationType.CUSTOM,
    ...(resultsCacheTtl ? { resultsCacheTtl } : undefined),
    ...(authorizerName ? { authorizerName } : undefined),
  };
};

export interface CkfRequestAuthorizer {
  authorizer: IAuthorizer;
  function: Function;
}

export const getRequestAuthorizer = (
  scope: Construct,
  appApiAuthorizerProps: CkfApiAuthorizerProps
): CkfRequestAuthorizer => {
  const requestAuthorizerProps: RequestAuthorizerProps = getAuthorizerProps(scope, appApiAuthorizerProps);
  return {
    authorizer: new RequestAuthorizer(scope, `${appApiAuthorizerProps.id}RequestAuthorizer`, {
      ...requestAuthorizerProps,
    }),
    function: requestAuthorizerProps.handler as Function,
  };
};

export interface CkfTokenAuthorizer {
  authorizer: IAuthorizer;
  function: Function;
}

export const getTokenAuthorizer = (
  scope: Construct,
  appApiAuthorizerProps: CkfApiAuthorizerProps
): CkfTokenAuthorizer => {
  const tokenAuthorizerConfig = appApiAuthorizerProps.config as TokenAuthorizerConfig;

  const validationRegex: string | undefined = tokenAuthorizerConfig.validationRegex
    ? tokenAuthorizerConfig.validationRegex
    : undefined;

  const tokenAuthorizerProps: TokenAuthorizerProps = getAuthorizerProps(scope, appApiAuthorizerProps);

  return {
    authorizer: new TokenAuthorizer(scope, `${appApiAuthorizerProps.id}TokenAuthorizer`, {
      ...tokenAuthorizerProps,
      ...(validationRegex ? { validationRegex } : undefined),
    }),
    function: tokenAuthorizerProps.handler as Function,
  };
};

export interface CkfCognitoAuthorizer {
  authorizer: IAuthorizer;
}

class InvalidNumberOfCognitoAuthHeaders extends Error {
  constructor(numberOfHeaders: number) {
    super(
      `Cognito authorizer requires exactly 1 authorization header. ${
        numberOfHeaders === -1 ? 'None' : numberOfHeaders
      } were received.`
    );
  }
}

class InvalidCognitoConfigId extends Error {
  constructor(providedId: string) {
    super(`The provided Cognito config id "${providedId}" does not exist on the API configuration.`);
  }
}

class MissingRequiredCognitoConfig extends Error {
  constructor() {
    super('Cognito authorizer config must be either a configured Id, or an ARN to an existing pool.');
  }
}

class InvalidCognitoConfig extends Error {
  constructor() {
    super('Cognito authorizer config must be either a configured Id, or an ARN to an existing pool - not both.');
  }
}

export const getCognitoAuthorizer = (
  scope: Construct,
  appApiAuthorizerProps: CkfApiAuthorizerProps
): CkfCognitoAuthorizer => {
  const cognitoAuthorizerConfig = appApiAuthorizerProps.config as CognitoAuthorizerConfig;
  if (!(cognitoAuthorizerConfig.cognitoConfigId || cognitoAuthorizerConfig.cognitoPoolArn)) {
    throw new MissingRequiredCognitoConfig();
  }
  if (cognitoAuthorizerConfig.cognitoConfigId && cognitoAuthorizerConfig.cognitoPoolArn) {
    throw new InvalidCognitoConfig();
  }

  if (appApiAuthorizerProps.authHeaders.length !== 1) {
    throw new InvalidNumberOfCognitoAuthHeaders(appApiAuthorizerProps.authHeaders.length);
  }
  const identitySource = appApiAuthorizerProps.authHeaders.map((authHeader) => IdentitySource.header(authHeader))[0];

  const appRestApi = appApiAuthorizerProps.appRestApi;

  const providerArns: string[] = [];
  if (cognitoAuthorizerConfig.cognitoConfigId) {
    const cognitoPool = appRestApi.cognitoPools[cognitoAuthorizerConfig.cognitoConfigId];
    if (!cognitoPool) {
      throw new InvalidCognitoConfigId(cognitoAuthorizerConfig.cognitoConfigId);
    }
    providerArns.push(cognitoPool.userPool.userPoolArn);
  } else {
    providerArns.push(cognitoAuthorizerConfig.cognitoPoolArn as string);
  }

  return {
    authorizer: {
      authorizerId: new CfnAuthorizer(scope, `${appApiAuthorizerProps.id}CognitoAuthorizer`, {
        restApiId: appRestApi.api.restApiId,
        type: AuthorizationType.COGNITO,
        identitySource,
        name: `${appApiAuthorizerProps.id}${cognitoAuthorizerConfig.cognitoConfigId || 'ByARN'}`,
        providerArns,
      }).ref,
      authorizationType: AuthorizationType.COGNITO,
    },
  };
};
