import { Construct } from '@aws-cdk/core';
import { AuthorizerConfig, AuthorizerType } from '../lib';
import { IAuthorizer } from '@aws-cdk/aws-apigateway';
import { getCognitoAuthorizer, getRequestAuthorizer, getTokenAuthorizer } from './lib';
import { CkfRestApi } from '../index';
import { Function } from '@aws-cdk/aws-lambda';
import { CkfNestedRestApi } from '../ckf-nested-rest-api';

export * from './lib';

class UnsupportedAuthorizerType extends Error {
  constructor(authorizerType: string) {
    super(
      `The specified authorizer type "${authorizerType}" is not supported. ` +
        `Supported authorizer types are: ${Object.values(AuthorizerType).join(', ')}.`
    );
  }
}

export interface CkfApiAuthorizerProps extends AuthorizerConfig {
  appRestApi: CkfRestApi | CkfNestedRestApi;
}

export class CkfApiAuthorizer extends Construct {
  authorizer: IAuthorizer;
  function?: Function;

  constructor(scope: Construct, id: string, props: CkfApiAuthorizerProps) {
    super(scope, id);

    switch (props.authorizerType) {
      case AuthorizerType.COGNITO:
        this.authorizer = getCognitoAuthorizer(scope, props).authorizer;
        break;
      case AuthorizerType.REQUEST:
        const requestAuthorizer = getRequestAuthorizer(scope, props);
        this.authorizer = requestAuthorizer.authorizer;
        this.function = requestAuthorizer.function;
        break;
      case AuthorizerType.TOKEN:
        const tokenAuthorizer = getTokenAuthorizer(scope, props);
        this.authorizer = tokenAuthorizer.authorizer;
        this.function = tokenAuthorizer.function;
        break;
      default:
        throw new UnsupportedAuthorizerType(props.authorizerType);
    }
  }
}
