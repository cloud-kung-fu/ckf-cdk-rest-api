import { Construct } from '@aws-cdk/core';
import { LambdaLayerConfig } from '../lib';
import { AssetCode, ILayerVersion, LayerVersion, Runtime } from '@aws-cdk/aws-lambda';

export interface LambdaLayerProps extends LambdaLayerConfig {}

export class LambdaLayer extends Construct {
  runtimes: Runtime[];
  layer: ILayerVersion;

  constructor(scope: Construct, id: string, props: LambdaLayerProps) {
    super(scope, id);

    this.runtimes = props.runtimes || [];

    const description = props.description;
    const layerVersionName = props.name;
    const license = props.license;

    this.layer = new LayerVersion(scope, `${props.id}Construct`, {
      code: new AssetCode(props.sourcePath),
      compatibleRuntimes: this.runtimes,
      ...(description ? { description } : undefined),
      ...(layerVersionName ? { layerVersionName } : undefined),
      ...(license ? { license } : undefined),
    });
  }
}
