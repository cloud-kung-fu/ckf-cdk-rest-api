import { Runtime } from '@aws-cdk/aws-lambda';

export type Id = string;

export interface CognitoPoolConfig {
  readonly id: Id;
  readonly minLength?: number;
  readonly userPoolId?: string;
  readonly userPoolClientId?: string;
}

export interface LambdaLayerConfig {
  readonly sourcePath: string;
  readonly runtimes: Runtime[];
  readonly id: Id;
  readonly description?: string;
  readonly license?: string;
  readonly name?: string;
}

export interface LambdaFunctionConfig {
  readonly sourcePath: string;
  readonly handlerFile: string;
  readonly handlerFunction: string;
  readonly runtime: Runtime;
  readonly layers?: Id[];
  readonly timeout?: number;
  readonly memorySize?: number;
}

export enum AuthorizerType {
  TOKEN = 'TOKEN',
  REQUEST = 'REQUEST',
  COGNITO = 'COGNITO',
}

interface LambdaAuthorizerConfig {
  readonly function: LambdaFunctionConfig;
  readonly resultsCacheTtlSeconds?: number;
}

export interface RequestAuthorizerConfig extends LambdaAuthorizerConfig {}

export interface TokenAuthorizerConfig extends LambdaAuthorizerConfig {
  readonly validationRegex?: string;
}

export interface CognitoAuthorizerConfig {
  cognitoConfigId?: Id;
  cognitoPoolArn?: string;
}

export interface AuthorizerConfig {
  readonly id: Id;
  readonly authorizerType: AuthorizerType;
  readonly config: RequestAuthorizerConfig | TokenAuthorizerConfig | CognitoAuthorizerConfig;
  readonly authHeaders: string[];
  readonly name?: string;
}

export const isAuthorizerConfig = (authorizerConfig: AuthorizerConfig | any): authorizerConfig is AuthorizerConfig => {
  return (
    typeof authorizerConfig === 'object' &&
    authorizerConfig.authorizerType &&
    authorizerConfig.id &&
    authorizerConfig.config &&
    authorizerConfig.authHeaders
  );
};

export interface CkfRestApiEndpointConfig {
  readonly function: LambdaFunctionConfig;
  readonly id: Id;
  readonly httpMethod: string;
  readonly apiPath: string;
  readonly authorizer?: AuthorizerConfig | Id;
}

export type CkfRestApiConfig = {
  readonly endpoints: CkfRestApiEndpointConfig[];
  readonly authorizers?: AuthorizerConfig[];
  readonly layers?: LambdaLayerConfig[];
  readonly cognitoPools?: CognitoPoolConfig[];
};
