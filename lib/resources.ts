import { CkfRestApiConfig } from './config';
import { getPathArraysFromPaths, PathArrays } from './path-arrays';
import { IResource } from '@aws-cdk/aws-apigateway';
import { addCorsOptions } from './add-cors-options';

interface ResourceTree {
  [node: string]: ResourceTree;
}

export const getResourceTreeFromPathArrays = (pathArrays: PathArrays): ResourceTree => {
  // TODO: Add validation to make sure any node set as a parameter does not have siblings
  const resourceTree: ResourceTree = {};
  if (!pathArrays.length) {
    return resourceTree;
  }
  // This avoids mutating the argument and filters out empty arrays
  const nextPathArrays = pathArrays.map((pathArray) => [...pathArray]).filter((pathArray) => pathArray.length);

  const currentNodePathArrays: { [node: string]: PathArrays } = {};
  nextPathArrays.forEach((pathArray) => {
    const currentNodeName = pathArray.shift() as string;
    if (!currentNodePathArrays[currentNodeName]) {
      currentNodePathArrays[currentNodeName] = [pathArray];
    } else {
      currentNodePathArrays[currentNodeName].push(pathArray);
    }
  });

  Object.entries(currentNodePathArrays).forEach(([node, pathArrays]) => {
    resourceTree[node] = getResourceTreeFromPathArrays(pathArrays);
  });

  return resourceTree;
};

export const getResourceTreeFromConfig = (apiConfig: CkfRestApiConfig): ResourceTree =>
  getResourceTreeFromPathArrays(
    getPathArraysFromPaths(apiConfig.endpoints.map((endpointConfig) => endpointConfig.apiPath))
  );

export const createResourcesForResourceTree = (root: IResource, resourceTree: ResourceTree) => {
  Object.entries(resourceTree).forEach(([resource, subTree]) => {
    const newRoot = root.addResource(resource);
    addCorsOptions(newRoot);
    createResourcesForResourceTree(newRoot, subTree);
  });
};

export const createResourcesFromConfig = (apiConfig: CkfRestApiConfig, resourceRoot: IResource) => {
  createResourcesForResourceTree(resourceRoot, getResourceTreeFromConfig(apiConfig));
};
