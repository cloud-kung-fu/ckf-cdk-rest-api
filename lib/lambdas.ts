import { LambdaFunctionProps } from '../lambda-function';
import { LambdaFunctionConfig } from './config';
import { CkfRestApi } from '../index';
import { CkfNestedRestApi } from '../ckf-nested-rest-api';

export const getLambdaFunctionProps = (
  functionConfig: LambdaFunctionConfig,
  appRestApi: CkfRestApi | CkfNestedRestApi
): LambdaFunctionProps => {
  return {
    handlerFile: functionConfig.handlerFile,
    handlerFunction: functionConfig.handlerFunction,
    runtime: functionConfig.runtime,
    sourcePath: functionConfig.sourcePath,
    ...(functionConfig.layers
      ? {
          layers: functionConfig.layers.map((layerId) => {
            if (!appRestApi.layers[layerId]) {
              throw new Error();
            }
            return appRestApi.layers[layerId];
          }),
        }
      : undefined),
    ...(functionConfig.timeout ? { timeout: functionConfig.timeout } : undefined),
    ...(functionConfig.memorySize ? { memorySize: functionConfig.memorySize } : undefined),
  };
};
