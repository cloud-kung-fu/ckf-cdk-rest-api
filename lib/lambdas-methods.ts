import { CkfRestApiConfig, CkfRestApiEndpointConfig, AuthorizerConfig, Id, isAuthorizerConfig } from './config';
import { IResource, MethodOptions } from '@aws-cdk/aws-apigateway';
import { getPathArrayFromPath } from './path-arrays';
import { LambdaFunction } from '../lambda-function';
import { CkfRestApi } from '../index';
import { CkfApiAuthorizer } from '../ckf-api-authorizer';
import { getLambdaFunctionProps } from './lambdas';
import { CkfNestedRestApi } from '../ckf-nested-rest-api';

class InvalidAuthorizerId extends Error {
  constructor(providedId: string) {
    super(`The provided authorizer id "${providedId}" does not exist on the API configuration.`);
  }
}

export const createLambdaAndMethodFromEndpointConfig = (
  endpointConfig: CkfRestApiEndpointConfig,
  appRestApi: CkfRestApi | CkfNestedRestApi
) => {
  const pathArray = getPathArrayFromPath(endpointConfig.apiPath);
  const root = appRestApi.api.root;

  const targetResource = pathArray.reduce(
    (previousResource, nextPath) => previousResource.getResource(nextPath) as IResource,
    root
  );

  const lambda = new LambdaFunction(
    appRestApi,
    `${endpointConfig.id}Lambda`,
    getLambdaFunctionProps(endpointConfig.function, appRestApi)
  );
  appRestApi.endpointLambdaFunctions[endpointConfig.id] = lambda;

  let authorizer: CkfApiAuthorizer | undefined;
  if (endpointConfig.authorizer) {
    if (isAuthorizerConfig(endpointConfig.authorizer)) {
      const authorizerConfig = endpointConfig.authorizer as AuthorizerConfig;
      authorizer = new CkfApiAuthorizer(appRestApi, `${endpointConfig.id}AuthConstruct`, {
        ...authorizerConfig,
        appRestApi,
      });
    } else {
      const id = endpointConfig.authorizer as Id;
      authorizer = appRestApi.authorizers[id];
      if (!authorizer) {
        throw new InvalidAuthorizerId(id);
      }
    }
  }

  const authorizerProps: MethodOptions | undefined = authorizer
    ? {
        authorizationType: authorizer.authorizer.authorizationType,
        authorizer: authorizer.authorizer,
      }
    : undefined;

  targetResource.addMethod(endpointConfig.httpMethod, lambda.integration, { ...authorizerProps });
};

export const createLambdasAndMethodsFromConfig = (
  apiConfig: CkfRestApiConfig,
  appRestApi: CkfRestApi | CkfNestedRestApi
) => {
  apiConfig.endpoints.forEach((apiEndpointConfig) => {
    createLambdaAndMethodFromEndpointConfig(apiEndpointConfig, appRestApi);
  });
};
