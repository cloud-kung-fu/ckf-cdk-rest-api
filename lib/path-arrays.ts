type PathArray = string[];
export type PathArrays = PathArray[];

export const getPathArrayFromPath = (path: string): PathArray => path.split('/');

export const getPathArraysFromPaths = (paths: string[]): PathArrays => paths.map((path) => getPathArrayFromPath(path));
