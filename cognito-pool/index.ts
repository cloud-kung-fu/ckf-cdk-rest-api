import { Construct } from '@aws-cdk/core';
import { IUserPool, IUserPoolClient, UserPool, UserPoolClient } from '@aws-cdk/aws-cognito';
import { CognitoPoolConfig } from '../lib';

export interface CognitoPoolProps extends CognitoPoolConfig {
  // TODO: Add props to allow more configuration
  stage: string;
}

export class CognitoPool extends Construct {
  userPool: IUserPool;
  userPoolClient: IUserPoolClient;

  constructor(scope: Construct, id: string, props: CognitoPoolProps) {
    super(scope, id);

    const { minLength, userPoolClientId, userPoolId, stage } = props;

    const userPoolConstructId = `${props.id}UserPool`;
    const userPool: IUserPool = (this.userPool = !userPoolId
      ? new UserPool(scope, userPoolConstructId, {
          signInAliases: { email: true, username: false },
          ...(minLength ? { passwordPolicy: { minLength } } : undefined),
        })
      : UserPool.fromUserPoolId(scope, userPoolConstructId, userPoolId));

    const userPoolClientConstructId = `${id}UserPoolClient`;
    this.userPoolClient = !userPoolClientId
      ? new UserPoolClient(scope, userPoolClientConstructId, {
          userPool,
          userPoolClientName: `${stage}-${props.id}`,
        })
      : UserPoolClient.fromUserPoolClientId(scope, userPoolClientConstructId, userPoolClientId);
  }
}
