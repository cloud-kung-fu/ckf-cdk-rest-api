import { Construct, Duration } from '@aws-cdk/core';
import { AssetCode, Function, Runtime } from '@aws-cdk/aws-lambda';
import { LambdaIntegration } from '@aws-cdk/aws-apigateway';
import { LambdaLayer } from '../lambda-layer';

export interface LambdaFunctionProps {
  sourcePath: string;
  handlerFile: string;
  handlerFunction: string;
  runtime: Runtime;
  layers?: LambdaLayer[];
  timeout?: number;
  memorySize?: number;
}

class LayerDoesNotSupportFunctionRuntime extends Error {
  constructor(functionRuntime: Runtime, layerRuntimes: Runtime[]) {
    super(
      `Layer does not support the function runtime. Function runtime is "${functionRuntime}". ` +
        `Layer runtimes are "${layerRuntimes.join(', ')}".`
    );
  }
}

export class LambdaFunction extends Construct {
  function: Function;
  code: AssetCode;
  integration: LambdaIntegration;
  runtime: Runtime;

  constructor(scope: Construct, id: string, props: LambdaFunctionProps) {
    super(scope, id);

    const runtime = (this.runtime = props.runtime);
    const code = (this.code = new AssetCode(props.sourcePath));
    const handler = `${props.handlerFile}.${props.handlerFunction}`;
    const memorySize = props.memorySize;
    const timeout = props.timeout ? Duration.seconds(props.timeout as number) : undefined;

    const layers = props.layers
      ? (props.layers as LambdaLayer[]).map((lambdaLayer) => {
          if (!lambdaLayer.runtimes.find((runtime) => this.runtime === runtime)) {
            throw new LayerDoesNotSupportFunctionRuntime(this.runtime, lambdaLayer.runtimes);
          }
          return lambdaLayer.layer;
        })
      : undefined;

    this.function = new Function(scope, `${id}Function`, {
      code,
      handler,
      runtime,
      ...(layers ? { layers } : undefined),
      ...(memorySize ? { memorySize } : undefined),
      ...(timeout ? { timeout } : undefined),
    });

    this.integration = new LambdaIntegration(this.function);
  }
}
