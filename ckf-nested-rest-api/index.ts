import { Construct } from '@aws-cdk/core';
import { NestedStack } from '@aws-cdk/aws-cloudformation';
import { IRestApi, RestApi } from '@aws-cdk/aws-apigateway';
import { CkfRestApi, CkfRestApiAuthorizers, EnvironmentVariables } from '../index';
import { LambdaLayer } from '../lambda-layer';
import { LambdaFunction } from '../lambda-function';
import { CognitoPool } from '../cognito-pool';
import { CkfRestApiConfig, createLambdasAndMethodsFromConfig, createResourcesFromConfig } from '../lib';

export interface CkfNestedApiProps {
  rootApi: CkfRestApi;
  config: CkfRestApiConfig;
  environmentVariables?: EnvironmentVariables[];
}

export class CkfNestedRestApi extends NestedStack {
  api: IRestApi;
  stage: string;
  authorizers: CkfRestApiAuthorizers;
  layers: { [layerId: string]: LambdaLayer };
  cognitoPools: { [cognitoPoolId: string]: CognitoPool };
  endpointLambdaFunctions: { [endpointId: string]: LambdaFunction };

  constructor(scope: Construct, id: string, props: CkfNestedApiProps) {
    super(scope, id);

    this.stage = props.rootApi.stage;
    this.authorizers = props.rootApi.authorizers;
    this.layers = props.rootApi.layers;
    this.cognitoPools = props.rootApi.cognitoPools;
    this.endpointLambdaFunctions = props.rootApi.endpointLambdaFunctions;

    const rootRestApi: RestApi = props.rootApi.api;
    this.api = RestApi.fromRestApiAttributes(this, `${id}NestedApi`, {
      restApiId: rootRestApi.restApiId,
      rootResourceId: rootRestApi.restApiRootResourceId,
    });

    this.configure(props.config);

    if (props.environmentVariables?.length) {
      Object.values(this.endpointLambdaFunctions).forEach((endpointLambdaFunction) => {
        props.environmentVariables?.forEach(({ key, value }) => {
          endpointLambdaFunction.function.addEnvironment(key, value);
        });
      });
    }
  }

  private configure(apiConfig: CkfRestApiConfig) {
    createResourcesFromConfig(apiConfig, this.api.root);
    createLambdasAndMethodsFromConfig(apiConfig, this);
  }
}
