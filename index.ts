import { Construct } from '@aws-cdk/core';
import { RestApi } from '@aws-cdk/aws-apigateway';
import {
  CkfRestApiConfig,
  createLambdasAndMethodsFromConfig,
  createResourcesFromConfig,
  isAuthorizerConfig,
} from './lib';
import { CkfApiAuthorizer } from './ckf-api-authorizer';
import { LambdaLayer } from './lambda-layer';
import { LambdaFunction } from './lambda-function';
import { CognitoPool } from './cognito-pool';

export class InvalidApiWideAuthorizerConfig extends Error {
  constructor() {
    super('Authorizers configured for reuse across the API must contain a full configuration.');
  }
}

export interface CkfRestApiAuthorizers {
  [id: string]: CkfApiAuthorizer;
}

export interface EnvironmentVariables {
  key: string;
  value: string;
}

export interface CkfRestApiProps {
  stage: string;
  id: string;
  name: string;
  config: CkfRestApiConfig;
  environmentVariables?: EnvironmentVariables[];
}

export class CkfRestApi extends Construct {
  api: RestApi;
  stage: string;
  authorizers: CkfRestApiAuthorizers;
  layers: { [layerId: string]: LambdaLayer };
  endpointLambdaFunctions: { [endpointId: string]: LambdaFunction };
  cognitoPools: { [cognitoPoolId: string]: CognitoPool };

  // TODO: Set up custom domains
  constructor(scope: Construct, id: string, props: CkfRestApiProps) {
    super(scope, id);

    this.stage = props.stage;

    this.api = new RestApi(scope, `${props.id}RestApi`, {
      restApiName: `${props.stage} ${props.name}`,
    });

    this.authorizers = {};
    this.layers = {};
    this.endpointLambdaFunctions = {};
    this.cognitoPools = {};

    this.configure(props.config);

    if (props.environmentVariables?.length) {
      Object.values(this.endpointLambdaFunctions).forEach((endpointLambdaFunction) => {
        props.environmentVariables?.forEach(({ key, value }) => {
          endpointLambdaFunction.function.addEnvironment(key, value);
        });
      });
    }
  }

  private configure(apiConfig: CkfRestApiConfig) {
    if (apiConfig.layers) {
      apiConfig.layers.forEach((layerConfig) => {
        this.layers[layerConfig.id] = new LambdaLayer(this, `${layerConfig.id}LambdaLayer`, layerConfig);
      });
    }

    if (apiConfig.cognitoPools) {
      apiConfig.cognitoPools.forEach((cognitoPoolConfig) => {
        this.cognitoPools[cognitoPoolConfig.id] = new CognitoPool(this, `${cognitoPoolConfig.id}CognitoPool`, {
          stage: this.stage,
          ...cognitoPoolConfig
        });
      });
    }

    if (apiConfig.authorizers) {
      apiConfig.authorizers.forEach((authorizerConfig) => {
        if (!isAuthorizerConfig(authorizerConfig)) {
          throw new InvalidApiWideAuthorizerConfig();
        }

        this.authorizers[authorizerConfig.id] = new CkfApiAuthorizer(this, `${authorizerConfig.id}AuthConstruct`, {
          ...authorizerConfig,
          appRestApi: this,
        });
      });
    }

    createResourcesFromConfig(apiConfig, this.api.root);
    createLambdasAndMethodsFromConfig(apiConfig, this);
  }
}
